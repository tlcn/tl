package com.luxurywatches.mb;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.hibernate.validator.constraints.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.luxurywatches.DAO.UserDAO;
import com.luxurywatches.entities.Customer;
import com.luxurywatches.entities.User;
import com.luxurywatches.utils.DataConnect;

@ManagedBean(name = "userBean")
@SessionScoped
public class UserBean implements Serializable{

	private static final long serialVersionUID = 1L;
	//email is used to login to system
	@Email(message = "Email format is invalid.")
	private String email2;
	private String password;
	private String dbEmail;
	private String dbPassword;
	private boolean loggedIn;
	private User user = new User();
	private Customer customer = new Customer();
	private List<User> users = new ArrayList<User>();

	private static final Logger LOGGER = LoggerFactory.getLogger(UserBean.class);

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDbEmail() {
		return dbEmail;
	}

	public void setDbEmail(String dbEmail) {
		this.dbEmail = dbEmail;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public List<User> getUsers() {
		UserDAO dao = new UserDAO();
		users = dao.findAllUsers();

		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	/**
	 *
	 * @Register new Customer and account for this customer
	 */
	public String register(){
		UserDAO dao = new UserDAO();

		customer.setUser(this.user);
		//user.setAdmin(false);
		dao.register(this.customer, this.user);

		//take the newly created email via login form
		email2 = user.getEmail();
		//reset data
		user = new User();
		customer = new Customer();

		return "success";
	}

	/**
	 *
	 * @return email and password of User table from database
	 *
	 */
	public void dbData(String email) throws ClassNotFoundException {
		if (email != null) {
			String query = "select email,password from user where email = '" + email + "'";
			Connection connection = null;
			ResultSet resultSet = null;
			Statement statement = null;

			try {
				//open connection
				connection = DataConnect.getConnection();
				if (connection != null) {
					statement = connection.createStatement();
					resultSet = statement.executeQuery(query);
					resultSet.next();
					dbEmail = resultSet.getString("email");
					dbPassword = resultSet.getString("password");
				}
			} catch (SQLException sqle) {
				LOGGER.info("Exception Occured in the process :" + sqle.getMessage());
			}finally {
				if (resultSet != null) {
					try{
						resultSet.close();
					} catch (SQLException e){
						LOGGER.info(e.getMessage());
					}
				}
				if (statement != null) {
					try{
						statement.close();
					} catch (SQLException e){
						LOGGER.info(e.getMessage());
					}
				}
				if (connection != null) {
					DataConnect.close(connection);
				}
			}
		}
	}
	/**
	 *
	 * @Login functional
	 */
	public String login() throws ClassNotFoundException {
		//get email and password from database
		dbData(email2);
		//validate email and password
		if (email2 != null && password != null && email2.equals(dbEmail) && password.equals(dbPassword)) {
			loggedIn = true;
			return "success";
		}
		else if (!email2.equals(dbEmail)){
			// Add View Faces Message
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_INFO,
							"Email doesn't exists",
							"Please register new account"));
			return "fail";
		}
		else {
			// Add View Faces Message
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_WARN,
							"Incorrect Email or Passowrd",
							"Please enter correct email and Password"));
			return "fail";
		}
	}

	/**
	 * add new user account
	 */
	public String addUser() {
		UserDAO dao = new UserDAO();
		dao.addUser(this.user);
		user = new User();
		return "/pages/user/user.jsf?faces-redirect=true";
	}

	/**
	 * get edit action
	 */
	public String editAction(User user) {
		this.user = user;

		return "/pages/user/edit.jsf?faces-redirect=true";
	}

	/**
	 * update user account
	 */
	public String updateUser(){
		UserDAO dao = new UserDAO();
		System.out.println("LOggggggg:   " + user.getEmail() + user.getPassword() + user.isAdmin());
		dao.updateUser(this.user);
		System.out.println("LOggggggg:   " + user.getEmail() + user.getPassword() + user.isAdmin());
		this.user = new User();

		return "/pages/user/user.jsf?faces-redirect=true";
	}

	/**
	 * delete user account
	 */
	public void deleteUser(User user) {
		UserDAO dao = new UserDAO();
		dao.deleteUser(user);
	}
}
