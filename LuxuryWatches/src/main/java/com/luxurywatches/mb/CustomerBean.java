package com.luxurywatches.mb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.luxurywatches.DAO.CustomerDAO;
import com.luxurywatches.entities.Customer;
import com.luxurywatches.entities.User;

@ManagedBean(name = "customerBean")
@SessionScoped
public class CustomerBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private Customer customer = new Customer();
	private User user = new User();
	private List<Customer> customers = new ArrayList<Customer>();

	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<Customer> getCustomers() {
		CustomerDAO dao = new CustomerDAO();
		customers = dao.findAllCustomers();

		return customers;
	}
	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	/**
	 * add new customer
	 */
	public String addCustomer(){
		CustomerDAO dao = new CustomerDAO();
		customer.setUser(this.user);
		dao.addCustomer(this.customer);
		customer = new Customer();

		return "/pages/customer/customer.jsf?faces-redirect=true";
	}

	/**
	 * get edit action
	 */
	public String editAction(Customer customer){
		this.customer = customer;

		return "/pages/customer/edit.jsf?faces-redirect=true";
	}

	/**
	 * update customer
	 */
	public String updateCustomer(){
		CustomerDAO dao = new CustomerDAO();
		dao.updateCustomer(this.customer);
		customer = new Customer();

		return "/pages/customer/customer.jsf?faces-redirect=true";
	}

	/**
	 * delete customer
	 */
	public void deleteCustomer(Customer customer) {
		CustomerDAO dao = new CustomerDAO();
		dao.deleteCustomer(customer);
	}
}
