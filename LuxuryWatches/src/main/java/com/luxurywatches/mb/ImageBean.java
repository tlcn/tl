package com.luxurywatches.mb;

import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.UploadedFile;

import com.luxurywatches.utils.DataConnect;

@ManagedBean(name = "imageBean")
@RequestScoped
public class ImageBean {
	UploadedFile file;

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	/**
	 * Store image in the database
	 */
	public void storeImage() {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			//connect to the database
			connection = DataConnect.getConnection();
			// Set autocommit to false to manage it by hand
			connection.setAutoCommit(false);

			// Create the statement object
			statement = connection.prepareStatement("INSERT INTO image(image)VALUES (?)");
			// Set file data
			statement.setBinaryStream(1, file.getInputstream());

			// Insert data to the database
			statement.executeUpdate();

			// Commit & close
			connection.commit();    // when autocommit=false
			connection.close();

			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Upload success", file.getFileName() + " is uploaded.");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (Exception e) {
			e.printStackTrace();

			// Add error message
			FacesMessage errorMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Upload error", e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, errorMsg);
		}
	}
}
