package com.luxurywatches.mb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.UploadedFile;

import com.luxurywatches.DAO.ProductDAO;
import com.luxurywatches.entities.Category;
import com.luxurywatches.entities.Product;
import com.luxurywatches.entities.Supplier;

@ManagedBean(name = "productBean")
@SessionScoped
public class ProductBean implements Serializable{

	private static final long serialVersionUID = 1L;
	private UploadedFile file;
	private Product product = new Product();
	private Category category = new Category();
	private Supplier supplier = new Supplier();
	private List<Product> products = new ArrayList<Product>();

	public UploadedFile getUploadedFile() {
		return file;
	}
	public void setUploadedFile(UploadedFile file) {
		this.file = file;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	public List<Product> getProducts() {
		ProductDAO dao = new ProductDAO();
		products = dao.findAllProducts();

		return products;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}

	/**
	 * add new product
	 */
	public String addProduct(){
		ProductDAO dao = new ProductDAO();
		product.setCategory(this.category);
		product.setSupplier(this.supplier);
		dao.addProduct(this.product);
		product = new Product();

		return "/pages/product/product.jsf?faces-redirect=true";
	}

	/**
	 * get edit action
	 */
	public String editAction(Product product){
		this.product = product;
		return "/pages/product/edit.jsf?faces-redirect=true";
	}

	/**
	 * update product
	 */
	public String updateProduct() {
		ProductDAO dao = new ProductDAO();
		dao.updateProduct(this.product);
		product = new Product();

		return "/pages/product/product.jsf?faces-redirect=true";
	}

	/**
	 * delete product
	 */
	public void deleteProduct(Product product) {
		ProductDAO dao = new ProductDAO();
		dao.deleteProduct(product);
	}
}
