package com.luxurywatches.mb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.luxurywatches.DAO.CategoryDAO;
import com.luxurywatches.entities.Category;

@ManagedBean(name = "categoryBean")
@SessionScoped
public class CategoryBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private Category category = new Category();
	private List<Category> categories = new ArrayList<Category>();

	public List<Category> getCategories() {
		CategoryDAO dao = new CategoryDAO();
		categories = dao.findAllCategories();

		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}

	/**
	 * save new category
	 */
	public String addCategory(){
		CategoryDAO dao = new CategoryDAO();
		dao.addCategory(this.category);
		//reset data
		category = new Category();
		return "success";
	}

	/**
	 * get edit action
	 */
	public String editAction(Category category){
		this.category = category;

		return "/pages/category/edit.jsf?faces-redirect=true";
	}

	/**
	 * update categody
	 */
	public String updateCategory() {
		CategoryDAO dao = new CategoryDAO();
		dao.updateCategory(this.category);
		//reset data
		this.category = new Category();
		return "/pages/category/category.jsf?faces-redirect=true";
	}

	/**
	 * delete category
	 */
	public void deleteCategory(Category category) {
		CategoryDAO dao = new CategoryDAO();
		dao.deleteCategory(category);
	}

}
