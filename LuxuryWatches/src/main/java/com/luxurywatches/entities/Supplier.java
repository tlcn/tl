package com.luxurywatches.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "supplier")
public class Supplier implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	private int supplierId;
	private String companyName;
	private String address;
	private String country;
	private String phone;
	private String email;
	private String fax;
	private String url;
	@OneToMany(mappedBy = "supplier")
	private Set<Product> products;

	public Supplier() {

	}

	public Supplier(String companyName, String address, String country,
			String phone, String email, String fax, String url) {
		this.companyName = companyName;
		this.address = address;
		this.country = country;
		this.phone = phone;
		this.email = email;
		this.fax = fax;
		this.url = url;
	}

	public int getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}


}
