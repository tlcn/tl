package com.luxurywatches.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.validator.constraints.Email;

import com.luxurywatches.validator.EmailExistsConstraint;

@Entity
@Table(name = "User", uniqueConstraints = @UniqueConstraint(columnNames = {"email"}))
public class User implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "userId")
	private int userId;
	@Email(message = "Email format is invalid.")
	@Column(name = "email", unique = true)
	@EmailExistsConstraint
	private String email;
	@Column(name = "password")
	private String password;
	@Column(name = "isAdmin")
	private boolean isAdmin;
	@OneToMany(mappedBy = "user")
	private Set<Customer> customers;

	public User() {
		//do not delete
	}

	public User(String email, String password, boolean isAdmin) {
		this.email = email;
		this.password = password;
		this.isAdmin = isAdmin;
	}

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public Set<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}

}
