package com.luxurywatches.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Order")
public class Order implements Serializable{

	private static final long serialVersionUID = 1L;

	private int orderId;
	private Date orderDate;
	private Double total_amount;
	private boolean paid;
	@ManyToOne
	@JoinColumn(name = "customerId")
	private Customer customer;
	@OneToMany(mappedBy = "OrderDetail")
	private Set<OrderDetail> orderDetails;

	public Set<OrderDetail> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(Set<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public Order() {

	}

	public Order(Date orderDate, Double total_amount, boolean paid,
			Customer customer) {
		this.orderDate = orderDate;
		this.total_amount = total_amount;
		this.paid = paid;
		this.customer = customer;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Double getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(Double total_amount) {
		this.total_amount = total_amount;
	}

	public boolean isPaid() {
		return paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

}
