package com.luxurywatches.services;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.luxurywatches.utils.DataConnect;

/**
 * Servlet implementation class ImageServlet
 */
@WebServlet(name = "ImageServlet", urlPatterns = { "/ImageServlet" })
public class ImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ImageServlet() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletresponseonse responseonse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Get last uploaded image
		try {
			// Image bytes
			byte[] imageBytes = null;

			// Connect to the database
			Connection connection = DataConnect.getConnection();

			// Create the statement
			// This query is specific to MySQL, it returns only one row (using 'LIMIT 1') - the last uploaded file
			PreparedStatement statement = connection.prepareStatement("SELECT id, image FROM image ORDER BY id DESC LIMIT 1");

			ResultSet rs = statement.executeQuery();

			while (rs.next()) { // This will run only once
				imageBytes = rs.getBytes("image");
			}

			// Close the connection
			connection.close();

			response.getOutputStream().write(imageBytes);
			response.getOutputStream().close();

		} catch (Exception e) {
			// Display error message
			response.getWriter().write(e.getMessage());
			response.getWriter().close();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletresponseonse responseonse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
